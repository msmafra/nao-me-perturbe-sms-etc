# Não me Perturbe
Para parar de receber, mesmo que temporariamente, ligações e SMS de telemarketing.
* [Chamadas Indesejadas - Anatel edita medida cautelar para combate a chamadas de robocall](https://www.gov.br/anatel/pt-br/consumidor/destaques/anatel-edita-medida-cautelar-para-combate-a-chamadas-de-robocall)

## Cadastre-se no PROCON de seu estado ou na Anatel para não ser perturbado por Telemarketing (ligações e SMS)

* **Teoricamente esta já resolveria de todos os casos, caso não, tente o de seu estado na lista.** [Anatel: Telemarketing Telecomunicações E Bancos Consignado - Não me perturbe](https://www.naomeperturbe.com.br/);

## Procons

Alguns sites de Procons estaduais também possuem uma forma de se cadastrar.

### Lista (até o momento)

* [PROCON SP](https://bloqueio.procon.sp.gov.br/#/);
* [PROCON GO](https://proconweb.ssp.go.gov.br/);
* [PROCON SC](https://bloqueiotelemarketing.procon.sc.gov.br/);
* [PROCON PR](https://www.bloqueio.procon.pr.gov.br/bloqueiotelemarketing/loginAcessoRestrito.do);
* [PROCON DF](https://merespeite.procon.df.gov.br/);
* [PROCON TO](http://bloqueios.procon.to.gov.br/users/sign_in);
* [PROCON RS](http://www.proconbloqueio.rs.gov.br/);
* [PROCON MG](https://aplicacao.mpmg.mp.br/proconbloqueio/);
* [PROCON MS](http://www.bloqtel.ms.gov.br/);
* [PROCON ES Instruções](https://procononline.com.br/procon-es/nao-me-importune/) - [Formulário](https://sistemas.es.gov.br/procon/bloqueiotelef/)
...
